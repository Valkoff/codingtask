<?php


namespace App\Service\Trip;


class Position
{
    public $latitude;
    public $longitude;

    public function __construct(float $latitude, float $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public function lat($radians = true): float
    {
        return $radians ? deg2rad($this->latitude) : $this->latitude;
    }

    public function lon($radians = true): float
    {
        return $radians ? deg2rad($this->longitude) : $this->longitude;
    }
}
