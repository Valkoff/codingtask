<?php


namespace App\Service\Trip\Score;

class EvenDistributionScore implements TripScoreInterface
{
    /**
     * @param  int  $minutes
     * @return int
     */
    public static function calculate(int $minutes): int
    {
        if ($minutes > 30) {
            return 100;
        }
        return 100 * $minutes / 30;
    }
}
