<?php

namespace App\Tests\Service\Trip;

use App\Service\Trip\Position;
use App\Service\Trip\Score\EvenDistributionScore;
use App\Service\Trip\Score\LogScore;
use App\Service\Trip\Trip;
use App\Service\Trip\Type\GoogleDistanceTripType;
use App\Service\Trip\Type\GoogleTripType;
use App\Service\Trip\Type\StraightTripType;
use PHPUnit\Framework\TestCase;

class TripTest extends TestCase
{


    /**
     * Short distance
     */
    public function testShortRange(): void
    {
        $home = new Position(45.762782, 9.075202);
        $work = new Position(45.756397, 9.075528);
        $nearbyTrip = new Trip($home, $work);

        /**
         * Straight
         */
        $minutes = $nearbyTrip->estimatedScore(new StraightTripType);
        $googleDistance = $nearbyTrip->estimatedScore(new GoogleDistanceTripType);
        $google = $nearbyTrip->estimatedScore(new GoogleTripType);

        $this->assertEquals(1, $minutes);
        $this->assertLessThanOrEqual($googleDistance, $minutes);
        $this->assertEquals(3, $googleDistance);
        $this->assertLessThanOrEqual($googleDistance, $google);

        /**
         * Even Distribution
         */
        $score = $nearbyTrip->estimatedScore(new StraightTripType, new EvenDistributionScore);
        $scoreGoogleDistance = $nearbyTrip->estimatedScore(new GoogleDistanceTripType, new EvenDistributionScore);
        $scoreGoogle = $nearbyTrip->estimatedScore(new GoogleTripType, new EvenDistributionScore);

        $this->assertEquals(3, $score);
        $this->assertLessThanOrEqual($scoreGoogleDistance, $score);
        $this->assertEquals(10, $scoreGoogleDistance);
        $this->assertLessThanOrEqual($scoreGoogle, $google);

        /**
         * Log
         */
        $log = $nearbyTrip->estimatedScore(new StraightTripType, new LogScore);
        $logGoogleDistance = $nearbyTrip->estimatedScore(new GoogleDistanceTripType, new LogScore);
        $logGoogle = $nearbyTrip->estimatedScore(new GoogleTripType, new LogScore);
        $this->assertEquals(0, $log);
        $this->assertLessThanOrEqual($logGoogleDistance, $log);
        $this->assertEquals(1, $logGoogleDistance);
        $this->assertLessThanOrEqual($logGoogleDistance, $logGoogle);

    }

    /**
     * Medium distance
     */
    public function testMediumRange(): void
    {
        $home = new Position(45.762782, 9.075202);
        $work = new Position(45.766397, 9.260528);
        $trip = new Trip($home, $work);

        /**
         * Straight
         */
        $minutes = $trip->estimatedScore(new StraightTripType);
        $googleDistance = $trip->estimatedScore(new GoogleDistanceTripType);
        $google = $trip->estimatedScore(new GoogleTripType);

        $this->assertEquals(28, $minutes);
        $this->assertLessThanOrEqual($googleDistance, $minutes);
        $this->assertEquals(44, $googleDistance);
        $this->assertLessThanOrEqual($googleDistance, $google);

        /**
         * Even Distribution
         */
        $score = $trip->estimatedScore(new StraightTripType, new EvenDistributionScore);
        $scoreGoogleDistance = $trip->estimatedScore(new GoogleDistanceTripType, new EvenDistributionScore);
        $scoreGoogle = $trip->estimatedScore(new GoogleTripType, new EvenDistributionScore);

        $this->assertEquals(93, $score);
        $this->assertLessThanOrEqual($scoreGoogleDistance, $score);
        $this->assertEquals(100, $scoreGoogleDistance);
        $this->assertLessThanOrEqual($scoreGoogle, $google);

        /**
         * Log
         */
        $log = $trip->estimatedScore(new StraightTripType, new LogScore);
        $logGoogleDistance = $trip->estimatedScore(new GoogleDistanceTripType, new LogScore);
        $logGoogle = $trip->estimatedScore(new GoogleTripType, new LogScore);
        $this->assertEquals(4, $log);
        $this->assertLessThanOrEqual($logGoogleDistance, $log);
        $this->assertEquals(5, $logGoogleDistance);
        $this->assertLessThanOrEqual($logGoogleDistance, $logGoogle);

    }

    /**
     * Long distance (Como-Roma)
     */
    public function testLongRange()
    {
        $home = new Position(45.7720212, 9.0531474);
        $work = new Position(42.065744, 12.596305);
        $longTrip = new Trip($home, $work);

        /**
         * Straight
         */
        $minutes = $longTrip->estimatedScore(new StraightTripType);
        $googleDistance = $longTrip->estimatedScore(new GoogleDistanceTripType);
        $google = $longTrip->estimatedScore(new GoogleTripType);

        $this->assertEquals($minutes, 1000);
        $this->assertLessThanOrEqual($googleDistance, $minutes);
        $this->assertEquals($googleDistance, 1198);
        $this->assertLessThanOrEqual($googleDistance, $google);

        /**
         * Even Distribution
         */
        $score = $longTrip->estimatedScore(new StraightTripType, new EvenDistributionScore);
        $scoreGoogleDistance = $longTrip->estimatedScore(new GoogleDistanceTripType, new EvenDistributionScore);
        $scoreGoogle = $longTrip->estimatedScore(new GoogleTripType, new EvenDistributionScore);

        $this->assertEquals(100, $score);
        $this->assertLessThanOrEqual($scoreGoogleDistance, $score);
        $this->assertEquals(100, $scoreGoogleDistance);
        $this->assertLessThanOrEqual($scoreGoogleDistance, $scoreGoogle);

        /**
         * Log
         */
        $log = $longTrip->estimatedScore(new StraightTripType, new LogScore);
        $logGoogleDistance = $longTrip->estimatedScore(new GoogleDistanceTripType, new LogScore);
        $logGoogle = $longTrip->estimatedScore(new GoogleTripType, new LogScore);
        $this->assertEquals(9, $log);
        $this->assertLessThanOrEqual($logGoogleDistance, $log);
        $this->assertEquals(10, $logGoogleDistance);
        $this->assertLessThanOrEqual($logGoogleDistance, $logGoogle);

    }
}
