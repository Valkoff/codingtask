<?php


namespace App\Service\Trip\Type;

class GoogleTripType extends BaseGoogleTripType
{

    /**
     * @return int
     */
    public function getDuration(): int
    {
        $this->getDistance();
        return is_object($this->leg) ? $this->leg->duration->value / 60 : 0;
    }

}