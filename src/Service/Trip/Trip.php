<?php


namespace App\Service\Trip;

use App\Service\Trip\Score\TripScoreInterface;
use App\Service\Trip\Type\TripTypeInterface;


class Trip
{

    /**
     * @var Position
     */
    private $homeCoords;
    /**
     * @var Position
     */
    private $workCoords;

    /**
     * Trip constructor.
     * @param  Position  $homeCoords
     * @param  Position  $workCoords
     */
    public function __construct(Position $homeCoords, Position $workCoords)
    {
        $this->homeCoords = $homeCoords;
        $this->workCoords = $workCoords;
    }

    /**
     * @param  TripTypeInterface  $factory
     * @param  TripScoreInterface|null  $scoreFactory
     * @return int
     */
    public function estimatedScore(TripTypeInterface $factory, TripScoreInterface $scoreFactory = null): int
    {
        $factory->init($this->homeCoords, $this->workCoords);
        $duration = $factory->getDuration();
        if ($scoreFactory !== null) {
            return $scoreFactory->calculate($duration);
        }
        return $duration;
    }

}
