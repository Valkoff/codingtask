<?php


namespace App\Service\Trip\Type;

class StraightTripType extends BaseTripType
{


    public function getDuration(): int
    {
        $distance = $this->getDistance();
        return $distance / self::AVG_SPEED * 60;
    }

    public function getDistance(): float
    {

        $dLat = deg2rad($this->work->lat(false) - $this->home->lat(false));
        $dLon = deg2rad($this->work->lon(false) - $this->home->lon(false));

        $a = sin($dLat / 2) * sin($dLat / 2) + sin($dLon / 2) * sin($dLon / 2)
            * cos($this->home->lat()) * cos($this->work->lat());
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        return self::EARTH_RADIUS * $c;
    }

}