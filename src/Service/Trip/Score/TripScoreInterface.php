<?php


namespace App\Service\Trip\Score;


interface TripScoreInterface
{
    /**
     * @param  int  $minutes
     * @return int
     */
    public static function calculate(int $minutes): int;
}
