<?php


namespace App\Service\Trip\Type;


use App\Service\Trip\Position;

abstract class BaseTripType implements TripTypeInterface
{
    /**
     * @var Position
     */
    public $home;
    /**
     * @var Position
     */
    public $work;

    public function init(Position $home, Position $work): void
    {
        $this->home = $home;
        $this->work = $work;
    }

}