<?php


namespace App\Service\Google;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class DirectionsApi
{
    public $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://maps.googleapis.com/maps/api/directions/',
            'verify' => false,
        ]);
    }

    public function getData($origin, $destination)
    {
        try{
            /**
             * origin=Toronto&destination=Montreal&key=AIzaSyB1xx8sKP15UYOB9qS6meJaTrbliuvA708
             */
            $parameters = [
                'origin' => $origin,
                'destination' => $destination,
                'key' => 'AIzaSyB1xx8sKP15UYOB9qS6meJaTrbliuvA708'
            ];

            $data = [
                'query' => $parameters
            ];

            $response = $this->client->request('GET', 'json', $data);
            $jsonResponse = json_decode($response->getBody(), false);
            if(is_object($jsonResponse) && property_exists($jsonResponse, 'routes')){
                $route = array_shift($jsonResponse->routes);
                return array_shift($route->legs);
            }
        }
        catch (ClientException $exception)
        {
            /**
             * Report and collect exception
             */
            return null;
        }
        return null;
    }
}