<?php


namespace App\Service\Trip\Score;


class LogScore implements TripScoreInterface
{

    private const LOG_BASE = 2;

    /**
     * @param  int  $minutes
     * @return int
     */
    public static function calculate(int $minutes): int
    {
        return $minutes >= 1 ? log($minutes, self::LOG_BASE) : 0;
    }
}