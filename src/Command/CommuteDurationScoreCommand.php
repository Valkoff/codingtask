<?php

namespace App\Command;


use App\Service\Trip\Position;
use App\Service\Trip\Score\EvenDistributionScore;
use App\Service\Trip\Trip;
use App\Service\Trip\Type\GoogleDistanceTripType;
use App\Service\Trip\Type\GoogleTripType;
use App\Service\Trip\Type\StraightTripType;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CommuteDurationScoreCommand extends Command
{
    protected static $defaultName = 'CommuteDurationScore';

    protected function configure()
    {
        $this
            ->setDescription('Tests the Trip class')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $home = new Position(45.766782, 9.075202);
        $work = new Position(45.766397, 9.260528);
        $trip = new Trip($home, $work);

        $straightTrip = new StraightTripType();
        $googleDistanceTrip = new GoogleDistanceTripType();
        $googleTrip = new GoogleTripType();

        $io->success('Straigth:         '.$trip->estimatedScore($straightTrip).'min, '.$trip->estimatedScore($straightTrip, new EvenDistributionScore()));
        $io->success('GoogleDistance:   '.$trip->estimatedScore($googleDistanceTrip).'min, '.$trip->estimatedScore($googleDistanceTrip, new EvenDistributionScore()));
        $io->success('Google:           '.$trip->estimatedScore($googleTrip).'min, '.$trip->estimatedScore($googleTrip, new EvenDistributionScore()));
        return true;
    }
}
