<?php


namespace App\Service\Trip\Type;


use App\Service\Google\DirectionsApi;

abstract class BaseGoogleTripType extends BaseTripType
{
    protected $apiClient;
    protected $leg;

    /**
     * GoogleDistanceTripType constructor.
     */
    public function __construct()
    {
        $this->apiClient = new DirectionsApi();
    }

    /**
     * @return float
     */
    public function getDistance(): float
    {
        if ($this->leg === null) {
            $origin = implode(',', [$this->home->lat(false), $this->home->lon(false)]);
            $destination = implode(',', [$this->work->lat(false), $this->work->lon(false)]);
            $this->leg = $this->apiClient->getData($origin, $destination);
        }
        return is_object($this->leg) ? $this->leg->distance->value / 1000 : 0;
    }


}