<?php


namespace App\Service\Trip\Type;

use App\Service\Trip\Position;

interface TripTypeInterface
{

    /**
     * Km/h
     */
    public const AVG_SPEED = 30;

    /**
     * Km
     */
    public const EARTH_RADIUS = 6371;

    public function init(Position $home, Position $work): void;

    public function getDistance(): float;

    public function getDuration(): int;
}
