<?php


namespace App\Service\Trip\Type;

class GoogleDistanceTripType extends BaseGoogleTripType
{
    /**
     * @return int
     */
    public function getDuration(): int
    {
        $distance = $this->getDistance();
        return $distance / self::AVG_SPEED * 60;
    }

}